# jamwiki_ansible

## Description

This requires Ansible 2+.

This will perform the following high-level tasks:
1. Download and install the version of jamwiki specified
2. Create a MySQL database and user as specified
3. Install and configure nginx as a frontend

## Usage

First, deploy to your target host:

`ansible-playbook -i targethost, --extra-vars "jamwiki_version=1.3.2 mysql_database=jamwiki mysql_user=jamwiki mysql_password=password" site.yaml`

Now, you can access http://targethost/ and complete the jamwiki setup.
You will need to fill these values in:
* File-system directory: `/srv/jamwiki`
* Persistence: `External Database`
* Database type: `mysql`
* JDBC driver class: `com.mysql.jdbc.Driver`
* Database URL or JNDI DataSource name: `jdbc:mysql://localhost:3306/<DATABASE_NAME_FROM_ABOVE>`
* Database Username: `<DATABASE_USERNAME_FROM_ABOVE>`
* Database Password: `<DATABASE_PASSWORD_FROM_ABOVE>`
* Admin user login: `<you decide>`
* New password: `<you decide>`
* Confirm new password: `<you decide>`
